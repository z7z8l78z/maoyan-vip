package com.maoyan.maoyanvip.controller;

import com.maoyan.entity.Member;
import com.maoyan.maoyanvip.service.RegisterService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("register")
public class RegisterController {
    @Resource
    private RegisterService registerService;

    @RequestMapping("/")
    public void register(Member member) {
        registerService.register(member);
    }
}
