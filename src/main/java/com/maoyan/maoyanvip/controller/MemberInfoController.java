package com.maoyan.maoyanvip.controller;

import com.maoyan.entity.Member;
import com.maoyan.maoyanvip.service.MemberInfoService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("userInfo")
public class MemberInfoController {
    @Resource
    private MemberInfoService memberInfoService;

    @RequestMapping("detail")
    public Member selectById(Integer id) {
        return memberInfoService.selectById(id);
    }

    public void updateById(Member user) {
        Member member = new Member();
        member.setPhonenum(user.getPhonenum());
        member.setBirth(user.getBirth());
        member.setEmail(user.getEmail());
        member.setMembernickname(user.getMembernickname());
        member.setSex(user.getSex());
        memberInfoService.updateById(member);
    }
}
