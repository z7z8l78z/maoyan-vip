package com.maoyan.maoyanvip;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MaoyanVipApplication {

    public static void main(String[] args) {
        SpringApplication.run(MaoyanVipApplication.class, args);
    }

}
