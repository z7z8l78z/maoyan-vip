package com.maoyan.maoyanvip.service;

import com.maoyan.entity.Member;
import com.maoyan.maoyanvip.dao.MemberInfoDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

@Service
@Transactional
public class MemberInfoServiceImpl implements MemberInfoService {
    @Resource
    private MemberInfoDao memberInfoDao;
    @Override
    public Member selectById(Integer id) {
        return memberInfoDao.selectById(id);
    }

    @Override
    public void updateById(Member member) {
        member.setOperatetime(new Date());
        memberInfoDao.updateById(member);
    }
}
