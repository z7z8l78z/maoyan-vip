package com.maoyan.maoyanvip.service;

import com.maoyan.entity.Member;
import org.springframework.stereotype.Service;

@Service
public interface MemberInfoService {
    Member selectById(Integer id);

    void updateById(Member member);
}
