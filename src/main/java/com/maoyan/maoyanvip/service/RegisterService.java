package com.maoyan.maoyanvip.service;

import com.maoyan.entity.Member;

public interface RegisterService {
    void register(Member member);
}
