package com.maoyan.maoyanvip.service;

import com.maoyan.entity.Member;
import com.maoyan.entity.MemberTotalIntegral;
import com.maoyan.maoyanvip.dao.RegisterDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

@Service
@Transactional
public class RegisterServiceImpl implements RegisterService {
    @Resource
    private RegisterDao registerDao;

    @Override
    public void register(Member member) {
        member.setCreatetime(new Date());
        registerDao.register(member);
        MemberTotalIntegral memberTotalIntegral = new MemberTotalIntegral();
        memberTotalIntegral.setMemberid(member.getId());
        registerDao.insertTotalIngetral(memberTotalIntegral);
    }
}
