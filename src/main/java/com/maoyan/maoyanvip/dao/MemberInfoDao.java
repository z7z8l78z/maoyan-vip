package com.maoyan.maoyanvip.dao;

import com.maoyan.entity.Member;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface MemberInfoDao {
    @Select("select phonenum,member_nickname,email,sex,birth,mti.member_total_integral as totalintegral from member m left join member_total_integral mti on m.id=mti.member_id where m.id = #{id}")
    @ResultMap("BaseResultMap")
    Member selectById(@Param("id") Integer id);

    void updateById(Member member);
}
