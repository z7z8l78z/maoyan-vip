package com.maoyan.maoyanvip.dao;

import com.maoyan.entity.Member;
import com.maoyan.entity.MemberTotalIntegral;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RegisterDao {
    void register(Member member);

    @Insert("insert into member_total_integral(member_id) values (#{memberid})")
    void insertTotalIngetral(MemberTotalIntegral memberTotalIntegral);
}
