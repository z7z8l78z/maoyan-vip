package com.maoyan.maoyanvip.service;

import com.maoyan.entity.Member;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RegisterServiceImplTest {

    @Resource
    private RegisterService registerService;

    @Test
    public void test(){
        Member member = new Member();
        member.setPhonenum("15670219651");
        registerService.register(member);
    }

}
